module "management_network" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  # source = "github.com/gruntwork-io/terraform-google-network.git//modules/vpc-network?ref=v0.1.2"
  source = "../"

  name_prefix = "test-vpc"
  project     = var.project
  region      = var.region

  private_service_connection = {
    address       = "10.108.16.0"
    prefix_length = 20
  }
}