output "network" {
  description = "A reference (self_link) to the VPC network"
  value       = module.management_network.network
}

output "network_name" {
  value = module.management_network.network_name
}

output "network_id" {
  value = module.management_network.network_id
}
